from random import randint
import os
gameTam = 5
#Gera um campo aleatoriamente
#
def serverGerarCampo(campo):
    #para cada tamanho de barco(do 2 ao 5)
    for i in range(2,6):
        #enquanto nao gerar coordenadas validas vai ficar no loop
        while True:
            #gera uma posicao aleatoria
            c1 = [randint(0, gameTam-1), randint(0, gameTam-1)]

            #direcao do barco
            #0 = cima ; 1 = direita; 2 = baixo; 3 = esquerda
            dir = randint(0,3)

            if dir == 0:#cima
                c2= [c1[0]-1, c1[1]]
            elif dir == 1: #direita
                c2 = [c1[0], c1[1]+1]
            elif dir == 2: #baixo
                c2 = [c1[0]+1, c1[1]]
            else: #esquerda
                c2 = [c1[0], c1[1]-1]
            if checkValInput(c1, c2, campo, i):
                posicionarBarco(c1, c2, campo, i)
                break

#Recebe as posicoes de todos os barcos
#o primeiro valor e onde a cabeca do barco vai ficar
#o segundo e a direcao do resto do barco
def playerInitCamp(campo,campoServer):
    numb = 2
    while numb < 6:
        while True:
            while True:
                print "Insira a primeira posicao do barco ", numb - 1, "que possui tamanho ", numb, ": "
                c1 = raw_input().split(" ")

                if not checkInput(c1):
                    print "Valor invalido, tente novamente."

                else:
                    break
            while True:
                print "Insira a segunda posicao do barco ", numb - 1, "que possui tamanho ", numb, ": "
                c2 = raw_input().split(" ")

                if not checkInput(c2):
                    print "Valor invalido, tente novamente."

                else:
                    break
            if not checkValInput(c1, c2, campo, numb):
                print "Nao e' possivel posicinar o barco neste lugar, aperte ENTER para inserir novamente os valores."
                raw_input()
            else:
                break
        posicionarBarco(c1, c2, campo, numb)
        imprimirCampo(campoPlayer, campoServer)
        numb += 1


#Posiciona o barco de tamanho informado no campo nas coordenadas
def posicionarBarco(var1,var2,campo,barc):
    for i in range(barc):
        if var1[0] == var2[0]: # <-->
            if var1[1] > var2[1]: # <--
                campo[var1[0]][var1[1]-i] = barc
            else: #-->
                campo[var1[0]][var1[1]+i] = barc
        else: #cima ou baixo
            if var1[0] > var2[0]: #cima
                campo[var1[0]-i][var1[1]] = barc
            else: #baixo
                campo[var1[0]+i][var1[1]] = barc


#Verifica se a entrada e valida
def checkInput(var):
    if len(var) != 2:
        return False
    for i in var:
        try:
            if int(i) > gameTam-1 or int(i) < 0:
                return False
        except:
            return False

    return True



#Verifica se e possivel inserir o barco naquela posicao
def checkValInput(var1, var2, campo, tam):
    var1[0]=int(var1[0])
    var1[1] = int(var1[1])
    var2[0] = int(var2[0])
    var2[1] = int(var2[1])
    vetPoss= [[var1[0], var1[1]+1], [var1[0], var1[1]-1], [var1[0]+1, var1[1]], [var1[0]-1, var1[1]]]
    if var1 == var2:
        return False

    if var2 not in vetPoss:
        return False

    #checagem se cabe
    if var1[0] == var2[0]:
        if var1[1] > var2[1]:
            if var1[1]-(tam-1) < 0:
                return False
        else:
            if var1[1]+(tam-1) > gameTam-1:
                return False
    else:
        if var1[0] > var2[0]:
            if var1[0]-(tam-1) < 0:
                return False
        else:
            if var1[0]+(tam-1) > gameTam-1:
                return False

    #checagem de colisao
    for i in range(tam):
        if var1[0] == var2[0]: # <-->
            if var1[1] > var2[1]: # <--
                if campo[var1[0]][var1[1]-i] != "-":
                    return False
            else: #-->
                if campo[var1[0]][var1[1]+i] != "-":
                    return False
        else: #cima ou baixo
            if var1[0] > var2[0]: #cima
                if campo[var1[0]-i][var1[1]] != "-":
                    return False
            else: #baixo
                if campo[var1[0]+i][var1[1]] != "-":
                    return False
    return True

#efetua a jogada nas coordenadas c
#se for vazio so marca um miss
#o vetor de barcos tem 4 posicoes uma pra cada barco
#ao acertar um barco incrementa na posicao respectiva daquele barco
#se o valor chegar ao valor correspondente ao tamanho do barco ( ex: o primeiro barco tem tamanho 2, se barco[0] chegar a 2
#significa que o barco de tamanho 2 foi destruido
def fazerJogada(c,campo,barcos):
    local = campo[int(c[0])][int(c[1])]
    #se a posicao estiver vazia retorna -1(significa q errou)
    #se nao incrementa o contador do respectivo barco
    if local == '-':
        return -1
    else:
        barcos[int(local) - 2] += 1

    #se o barco atingio o valor respectivo ao seu tamanho
    #retorna o numero daquele barco ( 2, 3, 4 ou 5)
    if barcos[int(local)-2] == local:
        return local
    else:#caso tenha apenas acertado o barco e nao destruido retorna 1
        return 1

#logica de mostrar os 2 campos
def imprimirCampo(player, server):
    os.system('clear')
    str= []
    for i in range(4*gameTam+1 - 16):
        str.append(" ")
        if i == (4*gameTam+1 - 16)/2:
            str.append("Campo do jogador")
    str.append("       ")
    for i in range(4*gameTam+1 - 19):
        str.append(" ")
        if i == (4*gameTam+1 - 19)/2:
            str.append("Campo do computador")
    print "".join(str)
    str=[]
    for i in range(4*gameTam+1):
        str.append("_")
    print "".join(str),
    str=[]
    print "       ",
    for i in range(4*gameTam+1):
        str.append("_")
    print "".join(str)
    for i in range(gameTam):
        for j in range(gameTam):
            if j == 0:
                print "|",
            print player[i][j],
            if j < gameTam-1:
                print "|",
            elif j == gameTam-1:
                print "|",
        print "       ",
        for j in range(gameTam):
            if j == 0:
                print "|",
            print server[i][j],
            if j < gameTam-1:
                print "|",
            elif j == gameTam-1:
                print "|"
    str = []
    for i in range(4 * gameTam + 1):
        str.append("_")
    print "".join(str),
    str = []
    print "       ",
    for i in range(4 * gameTam + 1):
        str.append("_")
    print "".join(str)
    print ""


#inicializa as matrizes
#OBS: QUANDO MUDAR PRA CLIENTE E SERVIDOR, CADA UM INICIALIZA AS SUAS MATRIZES
#A FUNCAO SO PRECISA TER 2 ENTRADAS
def initCampos(campoPlayer,campoServer,campoPlayerJogar,campoServerJogar):
    for i in range(gameTam):
        campoPlayer.append([])
        campoServer.append([])
        campoPlayerJogar.append([])
        campoServerJogar.append([])
        for j in range(gameTam):
            campoPlayer[i].append("-")
            campoServer[i].append("-")
            campoPlayerJogar[i].append("-")
            campoServerJogar[i].append("-")

#verifica se algum player destruiu todos os barcos
def checkWinner(barcosPlayer,barcosServer):
    p = True
    s = True
    for i in range(4):
        if barcosServer[i] != i+2:
            p = False
        if barcosPlayer[i] != i+2:
            s = False

    return p or s

#MAIN

campoPlayer = []
campoPlayerJogar = []
campoServer = []
campoServerJogar = []

barcosPlayer= [0,0,0,0]
barcosServer= [0,0,0,0]

initCampos(campoPlayer,campoServer,campoPlayerJogar,campoServerJogar)


print ".......Iniciando configuracao do jogo......."
print "Voce tem 4 barcos: (| 2 | 2 |) , (| 3 | 3 | 3|) , (| 4 | 4 | 4 | 4 |) , (| 5 | 5 | 5 | 5 | 5 |)"
print "Escolha a posicao de cada barco inserindo 2 cordenadas"

#playerInitCamp(campoPlayer,campoServer)
serverGerarCampo(campoPlayer)
serverGerarCampo(campoServer)

#imprimirCampo(campoPlayer,campoPlayerJogar)

#se houve vencedor
vencedor = False

#false e' o player, true e' o servidor
jogador = False
while not vencedor:

    if not jogador:
        print "E a a sua vez!"
        imprimirCampo(campoPlayer, campoPlayerJogar)
        while True:

            print "Faca uma jogada, indique as coordenadas: "
            c = raw_input().split(" ")
            if checkInput(c):
                break
            else:
                print "Jogada Invalida!!!"
                continue

        switch = fazerJogada(c, campoServer, barcosServer)

        if  switch == -1:#-1 e' miss
            campoPlayerJogar[int(c[0])][int(c[1])]="X"
        elif switch == 1:#1 e' acerto
            campoPlayerJogar[int(c[0])][int(c[1])]="O"
        else:#qualquer valor diferente e' destruiu o barco
            campoPlayerJogar[int(c[0])][int(c[1])]= "O"
            print "Voce destruiu um barco inimigo!"

        jogador = True
        imprimirCampo(campoPlayer, campoPlayerJogar)


    else:
        print "E a vez do servidor"
        imprimirCampo(campoServer, campoServerJogar)
        c = [randint(0,gameTam-1),randint(0,gameTam-1)]
        switch = fazerJogada(c, campoPlayer, barcosPlayer)
        if switch == -1:
            campoServerJogar[c[0]][c[1]]="X"
        elif switch == 1:
            campoServerJogar[c[0]][c[1]]="O"
        else:
            campoServerJogar[c[0]][c[1]] = "O"
            print "Voce destruiu um barco inimigo!"
        jogador = False
        imprimirCampo(campoServer, campoServerJogar)
    vencedor = checkWinner(barcosPlayer,barcosServer)
if jogador:
    print "O Player Ganhou!"
else:
    print "O Servidor Ganhou!"
