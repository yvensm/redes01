import socket
import os
from random import randint
import copy

gameTam=6



def serverGerarCampo(campo):
	#para cada tamanho de barco(do 2 ao 5)
	for i in range(2,6):
		#enquanto nao gerar coordenadas validas vai ficar no loop
		while True:
			#gera uma posicao aleatoria
			c1 = [randint(0, gameTam-1), randint(0, gameTam-1)]

			#direcao do barco
			#0 = cima ; 1 = direita; 2 = baixo; 3 = esquerda
			dir = randint(0,3)

			if dir == 0:#cima
				c2= [c1[0]-1, c1[1]]
			elif dir == 1: #direita
				c2 = [c1[0], c1[1]+1]
			elif dir == 2: #baixo
				c2 = [c1[0]+1, c1[1]]
			else: #esquerda
				c2 = [c1[0], c1[1]-1]
			if checkValInput(c1, c2, campo, i):
				posicionarBarco(c1, c2, campo, i)
				break



# Verifica se a entrada e' valida
def checkInput(var):
	if len(var) != 2:
		return False
	for i in var:
		try:
			if int(i) > (gameTam - 1) or int(i) < 0:
				return False
		except:
			return False
	return True


# Verifica se e' possivel inserir o barco naquela posicao
def checkValInput(var1, var2, campo, tam):
	var1[0] = int(var1[0])
	var1[1] = int(var1[1])
	var2[0] = int(var2[0])
	var2[1] = int(var2[1])
	vetPoss = [[var1[0], var1[1] + 1], [var1[0], var1[1] - 1], [var1[0] + 1, var1[1]], [var1[0] - 1, var1[1]]]
	if var1 == var2:
		return False

	if var2 not in vetPoss:
		return False

	# checagem se cabe
	if var1[0] == var2[0]:
		if var1[1] > var2[1]:
			if var1[1] - (tam - 1) < 0:
				return False
		else:
			if var1[1] + (tam - 1) > gameTam - 1:
				return False
	else:
		if var1[0] > var2[0]:
			if var1[0] - (tam - 1) < 0:
				return False
		else:
			if var1[0] + (tam - 1) > gameTam - 1:
				return False

	# checagem de colisao
	for i in range(tam):
		if var1[0] == var2[0]:  # <-->
			if var1[1] > var2[1]:  # <--
				if campo[var1[0]][var1[1] - i] != "-":
					return False
			else:  # -->
				if campo[var1[0]][var1[1] + i] != "-":
					return False
		else:  # cima ou baixo
			if var1[0] > var2[0]:  # cima
				if campo[var1[0] - i][var1[1]] != "-":
					return False
			else:  # baixo
				if campo[var1[0] + i][var1[1]] != "-":
					return False
	return True


def imprimirCampo(player, jogo):
	os.system('clear')
	str = []
	for i in range(4 * gameTam + 1 - 16):
		str.append(" ")
		if i == (4 * gameTam + 1 - 16) / 2:
			str.append("Campo do jogador")
	str.append("       ")
	for i in range(4 * gameTam + 1 - 19):
		str.append(" ")
		if i == (4 * gameTam + 1 - 19) / 2:
			str.append("Campo do computador")
	print "".join(str)
	str = []
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str),
	str = []
	print "       ",
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str)
	for i in range(gameTam):
		for j in range(gameTam):
			if j == 0:
				print "|",
			print player[i][j],
			if j < gameTam - 1:
				print "|",
			elif j == gameTam - 1:
				print "|",
		print "       ",
		for j in range(gameTam):
			if j == 0:
				print "|",
			print jogo[i][j],
			if j < gameTam - 1:
				print "|",
			elif j == gameTam - 1:
				print "|"
	str = []
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str),
	str = []
	print "       ",
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str)
	print ""


def initCampos(campoPlayer, campoJogar):
	for i in range(gameTam):
		campoPlayer.append([])
		campoJogar.append([])
		for j in range(gameTam):
			campoPlayer[i].append("-")
			campoJogar[i].append("-")


def playerInitCamp(campo, campoJogar):
	numb = 2
	while numb < 6:
		while True:
			while True:
				print "Insira a primeira posicao do barco ", numb - 1, "que possui tamanho ", numb, ": "
				c1 = raw_input().split(" ")

				if not checkInput(c1):
					print "Valor invalido, tente novamente."
				else:
					break
			while True:
				print "Insira a segunda posicao do barco ", numb - 1, "que possui tamanho ", numb, ": "
				c2 = raw_input().split(" ")

				if not checkInput(c2):
					print "Valor invalido, tente novamente."
				else:
					break
			if not checkValInput(c1, c2, campo, numb):
				print "Nao e' possivel posicinar o barco neste lugar, aperte ENTER para inserir novamente os valores."
				raw_input()
			else:
				break
		posicionarBarco(c1, c2, campo, numb)
		imprimirCampo(campoPlayer, campoJogar)
		numb += 1


def fazerJogada(c, campo):
	local = copy.deepcopy(campo[int(c[0])][int(c[1])])

	# se a posicao estiver vazia retorna -1(significa q errou)
	# se nao incrementa o contador do respectivo barco
	if local == '-':
		campo[int(c[0])][int(c[1])] = "X"
		return -1
	else:
		campo[int(c[0])][int(c[1])] = "O"
		#barcos[int(local) - 2] += 1
		return local
	# # se o barco atingio o valor respectivo ao seu tamanho
	# # retorna o numero daquele barco ( 2, 3, 4 ou 5)
	# if barcos[int(local) - 2] == local:
	# 	return local
	# else:  # caso tenha apenas acertado o barco e nao destruido retorna 1
	# 	return 1


def posicionarBarco(var1, var2, campo, barc):
	for i in range(barc):
		if var1[0] == var2[0]:  # <-->
			if var1[1] > var2[1]:  # <--
				campo[var1[0]][var1[1] - i] = barc
			else:  # -->
				campo[var1[0]][var1[1] + i] = barc
		else:  # cima ou baixo
			if var1[0] > var2[0]:  # cima
				campo[var1[0] - i][var1[1]] = barc
			else:  # baixo
				campo[var1[0] + i][var1[1]] = barc

def checkWinner(barcosPlayer,barcosServer):
	winner = 1

	for k in range(4):
		if barcosPlayer[k] != k+2:
			winner = 0

	if(winner == 0):
		for k in range(4):
			if barcosServer[k] != k+2:
				winner = -1

	return winner

def printBarcosServer(barcos):
	string= []
	for i in range(4):
		string.append("(|")
		for j in range(i+2):
			if(barcos[i-2] > 0):
				print ''
			string.append(i+2)
			if(j<i-1):
				string.append(" | ")
		string.append("|)")
		if(i<4-1):
			string.append(" , ")
	
	print "".join(string)

HOST = '127.0.0.1'     # Endereco IP do Servidor
PORT = 3001       # Porta que o Servidor esta
tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dest = (HOST, PORT)
tcp.connect(dest)

campoPlayer = []
campoPlayerJogar = []
barcosPlayer= [0,0,0,0]
barcosServer= [0,0,0,0]


initCampos(campoPlayer,campoPlayerJogar)
imprimirCampo(campoPlayer,campoPlayerJogar)

vencedor = -1

print "--------------Batalha Naval TCP--------------"
print ".......Iniciando configuracao do jogo........"
print "O jogo comecou!!"
print "Voce tem 4 barcos: (| 2 | 2 |) , (| 3 | 3 | 3|) , (| 4 | 4 | 4 | 4 |) , (| 5 | 5 | 5 | 5 | 5 |)"
print "Escolha a posicao de cada barco inserindo 2 cordenadas"


playerInitCamp(campoPlayer,campoPlayerJogar)
#serverGerarCampo(campoPlayer)


aviso = ''

while vencedor == -1:

	imprimirCampo(campoPlayer, campoPlayerJogar)
	print barcosServer
	print barcosPlayer
	print aviso

	print "E' a a sua vez!"
	while True:
		print "Faca uma jogada, indique as coordenadas: "
		c = raw_input().split(" ")
		
		if not checkInput(c):
			print "Jogada Invalida!!!"
			continue
		if campoPlayerJogar[int(c[0])][int(c[1])] != "-":
			print "Voce ja escolheu esta coordenada, jogue novamente!"
			continue
		else:
			break

	msg = c[0],c[1]

	tcp.send(' '.join(msg))

	data = tcp.recv(1024)
	switch = int(data)#fazerJogada(c, campoServer, barcosServer)

	if switch == -1:  # -1 e' miss
		campoPlayerJogar[int(c[0])][int(c[1])] = "X"
		aviso = "Voce errou!"
	# elif switch == 1:  # 1 e' acerto
	# 	campoPlayerJogar[int(c[0])][int(c[1])] = "O"
	else:  # qualquer valor diferente e' destruiu o barco
		campoPlayerJogar[int(c[0])][int(c[1])] = "O"
		barcosServer[switch-2]+=1
		aviso = "Voce acertou um navio inimigo!"
		if barcosServer[switch-2] == switch:
			aviso = "Voce destruiu o barco de tamanho",switch
		

	vencedor = checkWinner(barcosPlayer,barcosServer)
	
	if(vencedor == 0):
		break
	
	print "Vez do servidor."

	data = tcp.recv(1024)#recebe jogada
	c = data.split(' ')
	res = fazerJogada(c,campoPlayer)
	
	tcp.send(str(res))#responde se acertou
	
	if res != -1:
		barcosPlayer[res-2]+=1

	vencedor = checkWinner(barcosPlayer, barcosServer)
	

	tcp.send(str(vencedor))

print "Fim de jogo."
win = "Parabe'ns voce ganhou!!!" if vencedor==0 else "O Servidor ganhou!!!"
print win
msg = raw_input()
tcp.close()