import socket
from random import randint
import copy
import os

gameTam=6

def checkInput(var):
	if len(var) != 2:
		return False
	for i in var:
		try:
			if int(i) > gameTam - 1 or int(i) < 0:
				return False
		except:
			return False

	return True


# Verifica se e' possivel inserir o barco naquela posicao
def checkValInput(var1, var2, campo, tam):
	var1[0] = int(var1[0])
	var1[1] = int(var1[1])
	var2[0] = int(var2[0])
	var2[1] = int(var2[1])
	vetPoss = [[var1[0], var1[1] + 1], [var1[0], var1[1] - 1], [var1[0] + 1, var1[1]], [var1[0] - 1, var1[1]]]
	if var1 == var2:
		return False

	if var2 not in vetPoss:
		return False

	# checagem se cabe
	if var1[0] == var2[0]:
		if var1[1] > var2[1]:
			if var1[1] - (tam - 1) < 0:
				return False
		else:
			if var1[1] + (tam - 1) > gameTam - 1:
				return False
	else:
		if var1[0] > var2[0]:
			if var1[0] - (tam - 1) < 0:
				return False
		else:
			if var1[0] + (tam - 1) > gameTam - 1:
				return False

	# checagem de colisao
	for i in range(tam):
		if var1[0] == var2[0]:  # <-->
			if var1[1] > var2[1]:  # <--
				if campo[var1[0]][var1[1] - i] != "-":
					return False
			else:  # -->
				if campo[var1[0]][var1[1] + i] != "-":
					return False
		else:  # cima ou baixo
			if var1[0] > var2[0]:  # cima
				if campo[var1[0] - i][var1[1]] != "-":
					return False
			else:  # baixo
				if campo[var1[0] + i][var1[1]] != "-":
					return False
	return True




def initCampos(campoPlayer, campoJogar):
	for i in range(gameTam):
		campoPlayer.append([])
		campoJogar.append([])
		for j in range(gameTam):
			campoPlayer[i].append("-")
			campoJogar[i].append("-")



def serverGerarCampo(campo):
	#para cada tamanho de barco(do 2 ao 5)
	for i in range(2,6):
		#enquanto nao gerar coordenadas validas vai ficar no loop
		while True:
			#gera uma posicao aleatoria
			c1 = [randint(0, gameTam-1), randint(0, gameTam-1)]

			#direcao do barco
			#0 = cima ; 1 = direita; 2 = baixo; 3 = esquerda
			dir = randint(0,3)

			if dir == 0:#cima
				c2= [c1[0]-1, c1[1]]
			elif dir == 1: #direita
				c2 = [c1[0], c1[1]+1]
			elif dir == 2: #baixo
				c2 = [c1[0]+1, c1[1]]
			else: #esquerda
				c2 = [c1[0], c1[1]-1]
			if checkValInput(c1, c2, campo, i):
				posicionarBarco(c1, c2, campo, i)
				break


def fazerJogada(c, campo):
	local = copy.deepcopy(campo[int(c[0])][int(c[1])])

	# se a posicao estiver vazia retorna -1(significa q errou)
	# se nao incrementa o contador do respectivo barco
	if local == '-':
		campo[int(c[0])][int(c[1])] = "X"
		return -1
	else:
		campo[int(c[0])][int(c[1])] = "X"
		#barcos[int(local) - 2] += 1
		return local


def posicionarBarco(var1, var2, campo, barc):
	for i in range(barc):
		if var1[0] == var2[0]:  # <-->
			if var1[1] > var2[1]:  # <--
				campo[var1[0]][var1[1] - i] = barc
			else:  # -->
				campo[var1[0]][var1[1] + i] = barc
		else:  # cima ou baixo
			if var1[0] > var2[0]:  # cima
				campo[var1[0] - i][var1[1]] = barc
			else:  # baixo
				campo[var1[0] + i][var1[1]] = barc



def imprimirCampo(player, jogo):
	os.system('clear')

	str = []
	for i in range(4 * gameTam + 1 - 16):
		str.append(" ")
		if i == (4 * gameTam + 1 - 16) / 2:
			str.append("Campo do jogador")
	str.append("       ")
	for i in range(4 * gameTam + 1 - 19):
		str.append(" ")
		if i == (4 * gameTam + 1 - 19) / 2:
			str.append("Campo do computador")
	print "".join(str)
	str = []
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str),
	str = []
	print "       ",
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str)
	for i in range(gameTam):
		for j in range(gameTam):
			if j == 0:
				print "|",
			print player[i][j],
			if j < gameTam - 1:
				print "|",
			elif j == gameTam - 1:
				print "|",
		print "       ",
		for j in range(gameTam):
			if j == 0:
				print "|",
			print jogo[i][j],
			if j < gameTam - 1:
				print "|",
			elif j == gameTam - 1:
				print "|"
	str = []
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str),
	str = []
	print "       ",
	for i in range(4 * gameTam + 1):
		str.append("_")
	print "".join(str)
	print ""


def gerarJogada(campo):
	acerto = []
	for i in range(gameTam):
		for k in range(gameTam):
			if campo[i][k] == "O":
				acerto = [i,k]
	

	if len(acerto) == 0:
		cord = []
		cord.append(str(randint(0,gameTam-1)))
		cord.append(str(randint(0,gameTam-1)))
		return cord
	
	proximaJogada=[]
	proximaJogada = serachPointRec(acerto,campo)

	if(proximaJogada == -1):
		cord = []
		cord.append(str(randint(0,gameTam-1)))
		cord.append(str(randint(0,gameTam-1)))
		return cord
	else:
		return proximaJogada


def serachPointRec(c,campo):


	if c != -1:
		if(c[0]-1 > 0):
			if campo[c[0]-1][c[1]] == "O":
				return serachPointRec(list(c[0]-1,c[1]))
			elif campo[c[0]-1][c[1]] == "-":
				c=[]
				c.append(str(c[0]-1))
				c.append(str(c[1]))
				return c
		

		if(c[0]+1 < 6):
			if campo[c[0]+1][c[1]] == "O":
				return serachPointRec(list(c[0]+1,c[1]))
			elif campo[c[0]+1][c[1]] == "-":
				c=[]
				c.append(str(c[0]+1))
				c.append(str(c[1]))
				return c

		if(c[1]-1 > 0):
			if campo[c[0]][c[1]-1] == "O":
				return serachPointRec(list(c[0],c[1]-1))
			elif campo[c[0]][c[1]-1] == "-":
				c=[]
				c.append(str(c[0]))
				c.append(str(c[1]-1))
				return c

		if(c[1]+1 < 6):
			if campo[c[0]][c[1]+1] == "O":
				return serachPointRec(list(c[0],c[1]+1))
			elif campo[c[0]][c[1]+1] == "-":
				c=[]
				c.append(str(c[0]+1))
				c.append(str(c[1]))
				return c

	return -1


HOST = ''              # Endereco IP do Servidor
PORT = 3001           # Porta que o Servidor esta
tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
orig = (HOST, PORT)
tcp.bind(orig)
tcp.listen(1)

campoServer=[]
campoServerJogar=[]
serverBarcos=[0,0,0,0]

initCampos(campoServer,campoServerJogar)
serverGerarCampo(campoServer)

con, cliente = tcp.accept()
print ('Conectado por', cliente)
vencedor = -1

acertou=False

while vencedor == -1:
	imprimirCampo(campoServer, campoServerJogar)
	print("E' a vez do Cliente")

	c = con.recv(1024)
	resJogada = fazerJogada(c.split(" "),campoServer)

	con.send(str(resJogada))
	print "Vez do servidor."

	while True:
		jogada = [str(randint(0,gameTam-1)),str(randint(0,gameTam-1))]
		if campoServerJogar[int(jogada[0])][int(jogada[1])] == "-":
			break

	# jogada = gerarJogada(campoServerJogar)

	con.send(" ".join(jogada))

	msg = con.recv(1024)
	switch = int(msg)
	if switch == -1:  # -1 e' miss
		campoServerJogar[int(jogada[0])][int(jogada[1])] = "X"
		acerto = False
		# elif switch == 1:  # 1 e' acerto
		# campoPlayerJogar[int(c[0])][int(c[1])] = "O"
	else:# qualquer valor diferente e' destruiu o barco
		campoServerJogar[int(jogada[0])][int(jogada[1])] = "O"
		acerto = True
	
	vencedor= int(con.recv(1024))

	


	#     if not msg: break
	#     print (cliente, msg)

print ('Finalizando conexao do cliente', cliente)
con.close()
tcp.close()